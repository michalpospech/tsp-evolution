build: main.o representations.o map.o
	g++ -std=c++17 -O3 main.o representations.o map.o -o tsp-evolution

main.o:
	g++ -c  -std=c++17 ./TSPEvolution/TSPEvolution.cpp -o main.o

representations.o:
	g++ -c  -std=c++17 -O3 ./TSPEvolution/Representations.cpp -o representations.o

map.o:
	g++ -c  -std=c++17 -O3 ./TSPEvolution/Map.cpp -o map.o

doc:
	doxygen doxyconf