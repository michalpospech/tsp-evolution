# TSP Evolution

## About

Semestral project for NPRG041 class (C++ programming) at Charles University.

This project uses evolutionary algorithms to approximate solution of TSP. It is designed in such way that new representations, operators and behaviours (e.g. paralell computation of fitness) can be easily added, only requiring changing the `main` method and recompilation.

Implemented epresentations and operators are based on <https://iccl.inf.tu-dresden.de/w/images/b/b7/GA_for_TSP.pdf>

## How to build

Run 

     make

in root directory of the project, `tsp-evolution` executable along with object files will be created.

## How to run

After building the program run it by entering 

    tsp-evolution <input file> <population size> <number of generations> <crossover chance> <mutation chance> <crossover algorithm>

where:

-   input file is file containing pairs of floats, each line containins one pair, values separated by a comma (included python script can be used for generation of such files)
-   population size is number of individuals in population, must be higher than 10
-   number of generations must be at least 1
-   crossover chance and mutation chance must be between 0.0 and 1.0
-   crossover algorithm is either `opc` (one point crossover) or `pmx` (partially modified crossover) 

## Description of output

Output is in csv format with each line describing one generation in this format

    <average fitness>,<minimal (best) fitness>

## Documentation

Documentation can be generated using doxygen by running 

    make doc
