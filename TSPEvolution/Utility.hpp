#pragma once
#include <mutex>
#include <type_traits>
#include <memory>
/**
 * \brief Namespace containing utility classes
 */
namespace utilities
{
	/**
	 * \brief Thread safe random number generator
	 * 
	 * Wraps random generator access to be thread safe
	 * \tparam TRNG Random number generator type
	 */
	template <typename TRNG>
	class thread_safe_generator
	{
		TRNG generator_;
		mutable std::mutex lock_;
	public:
		/**
		 * \brief result_type of the random number generator
		 */
		using result_type = typename TRNG::result_type;

		/**
		 * \brief Gets the minimal value that can be returned
		 * \return Minamal value
		 */
		static constexpr result_type min()
		{
			return TRNG::min();
		}

		/**
		 * \brief Gets the maximal value that can be returned
		 * \return Maximal value
		 */
		static constexpr result_type max()
		{
			return TRNG::max();
		}

		/**
		 * \brief Overloaded () operator
		 *
		 *	Advances the generator state and returns generated value. Thread safe.
		 * \return Pseudo random number in [min(), max()] interval
		 */
		result_type operator()()
		{
			std::lock_guard<std::mutex> lock(lock_);
			return generator_();
		}
	};


	/**
	 * \brief Class wrapping shared_pointer to RNG so that it could be used in STL algorithms
	 * \tparam TGenerator Type of generator
	 */
	template <typename TGenerator>
	class generator_ptr
	{
		std::shared_ptr<TGenerator> ptr_;
	public:
		generator_ptr() : ptr_(std::make_shared<TGenerator>())
		{
		}

		/**
		* \brief result_type of the random number generator
		*/
		using result_type = typename TGenerator::result_type;

		/**
		* \brief Gets the minimal value that can be returned
		* \return Minamal value
		*/
		static constexpr result_type min()
		{
			return TGenerator::min();
		}

		/**
		* \brief Gets the maximal value that can be returned
		* \return Maximal value
		*/
		static constexpr result_type max()
		{
			return TGenerator::max();
		}

		/**
		* \brief Overloaded () operator
		*
		*	Advances the generator state and returns generated value. Thread safe.
		* \return Pseudo random number in [min(), max()] interval
		*/
		result_type operator()()
		{
			return ptr_->operator()();
		}
	};

	/**
	 * \brief Class providing same interface for getting as std::future
	 * \tparam TItem Type of item contained
	 */
	template <typename TItem>
	class fake_future
	{
	public:
		/**
		 * \brief Constructor
		 * \param item Value to contain
		 */
		fake_future(TItem&& item) : item_(std::forward<TItem>(item))
		{
		}

		/**
		 * \brief Method to get encapsulated value
		 * \return Value
		 */
		TItem&& get()
		{
			return std::move(item_);
		}

		fake_future& operator=(const fake_future&) = delete;
		fake_future(const fake_future&) = delete;
		fake_future(fake_future&&) = default;
		fake_future& operator=(fake_future&&) = default;
		~fake_future() = default;
	private:
		TItem item_;
	};

	/**
	 * \brief Class encapsulating simple sequential execution
	 */
	class simple_executor
	{
	public:
		/**
		 * \brief Method to run given callable
		 * \tparam TFunction Type of callable
		 * \tparam TArgs Type of arguments
		 * \param function Callable
		 * \param args Arguments
		 * \return Result of computation wrapped in fake_future
		 */
		template <typename TFunction, typename... TArgs>
		auto operator()(TFunction&& function, TArgs&& ... args) const
		{
			return fake_future<decltype(std::declval<TFunction>()(args...))>(function(std::forward<TArgs>(args)...));
		}
	};
}
