#pragma once
#include <vector>
#include <istream>
/**
 * \brief Namespace containing classes used for describing the enviroment
 */
namespace enviroment
{

	/**
	 * \brief Class describing a 2-d vector/point
	 */
	class point
	{
		double x_;
		double y_;
	public:
		/**
		 * \brief Constructor
		 * \param x X-coordinate
		 * \param y Y-coordinate
		 */
		point(double x, double y);

		/**
		 * \brief Method to get x-coordinate
		 * \return X-coordinate
		 */
		double get_x() const;
		/**
		* \brief Method to get y-coordinate
		* \return Y-coordinate
		*/
		double get_y() const;
	};

	

	/**
	 * \brief Class containing set of points
	 */
	class point_set
	{
		std::vector<point> points_;
	public:
		/**
		 * \brief Default constructor
		 */
		point_set();
		/**
		 * \brief Method to get size of the point set
		 * \return Size of point set
		 */
		std::size_t size() const;
		/**
		 * \brief Constructor
		 * \param points Vector of points
		 */
		point_set(std::vector<point> points);
		/**
		 * \brief Method to get Euclidean distance between points at given indices
		 * \param index1 Index 1 
		 * \param index2 Index 2
		 * \return Distance
		 */
		double get_distance(std::size_t index1, std::size_t index2) const;
		/**
		 * \brief Method to get length of circular path consisting of given vertices
		 * \param path Path
		 * \return Lenghts
		 */
		double get_path_length(const std::vector<std::size_t>& path) const;
		/**
		 * \brief Method that parses input stream into pointset
		 * \param input Input stream
		 * \return Point set
		 */
		static point_set parse(std::istream& input);

	};
}
