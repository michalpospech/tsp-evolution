﻿// TSPEvolution.cpp : Defines the entry point for the console application.
//
#include "Population.hpp"
#include "Map.hpp"
#include <iostream>
#include <memory>
#include <random>
#include <fstream>
#include "Utility.hpp"
#include "Algorithms.hpp"
#include "Representations.hpp"
#include "Loggers.hpp"
#include <string>
#include "Evolution.hpp"
using generator_t = std::mt19937_64;
using pop_t_ord = population::population<representations::ordinal>;
using pop_t_path = population::population<representations::path>;
using algorithm_t_ord = algorithms::renew_all_algorithm<
	utilities::simple_executor, algorithms::tournament_selector<generator_t>, algorithms::one_point_crossover<
		generator_t>, algorithms::vertex_switch<generator_t>, std::mt19937_64>;
using algorithm_t_path = algorithms::renew_all_algorithm<
	utilities::simple_executor, algorithms::tournament_selector<generator_t>, algorithms::pmx<
		generator_t>, algorithms::vertex_switch<generator_t>, std::mt19937_64>;

int main(int argc, char** argv)
{
	if (argc != 7)
	{
		std::cout << "Invalid number of arguments" << std::endl;
		return 1;
	}
	std::ifstream points_input(argv[1]);
	enviroment::point_set points;
	try
	{
		points = enviroment::point_set::parse(points_input);
	}
	catch (const std::exception& e)
	{
		std::cout << e.what() << std::endl;
		return 1;
	}
	points_input.close();

	std::size_t problem_size = points.size();
	double cross_chance, mut_chance;
	std::size_t pop_size, num_gen;
	try
	{
		cross_chance = std::stod(argv[4]);
		mut_chance = std::stod(argv[5]);
		pop_size = std::stoll(argv[2]);
		num_gen = std::stoll(argv[3]);;
	}
	catch (const std::exception& e)
	{
		std::cout << e.what() << std::endl;
		return 1;
	}
	if (cross_chance <= 0 || cross_chance > 1)
	{
		std::cout << "Invalid crossover chance" << std::endl;
		return 1;
	}
	if (mut_chance <= 0 || mut_chance > 1)
	{
		std::cout << "Invalid mutation chance" << std::endl;
		return 1;
	}
	if (pop_size < 10)
	{
		std::cout << "Invalid population side" << std::endl;
		return 1;
	}
	if (num_gen < 1)
	{
		std::cout << "Invalid number of generations" << std::endl;
		return 1;
	}

	std::string cross_alg = argv[6];
	generator_t gen;
	algorithms::vertex_switch<generator_t> mutator(problem_size, &gen);
	algorithms::tournament_selector<generator_t> selector(10, 0.5, &gen);
	algorithms::pmx<generator_t> pmx(&gen, problem_size);
	algorithms::one_point_crossover<generator_t> opc(&gen, problem_size);
	utilities::simple_executor exec;
	loggers::basic_logger logger(std::cout);

	std::unique_ptr<evolution::evolution_base> evolution_ptr;
	if (cross_alg == std::string("opc"))
	{
		algorithm_t_ord alg(&gen, cross_chance, mut_chance, exec, selector, opc, mutator);
		evolution_ptr = std::make_unique<evolution::evolution<
			algorithm_t_ord, population::population<representations::ordinal>, loggers::basic_logger>>(
			alg, logger, num_gen, pop_size, &points, &gen);
	}
	else if (cross_alg == std::string("pmx"))
	{
		algorithm_t_path alg(&gen, cross_chance, mut_chance, exec, selector, pmx, mutator);
		evolution_ptr = std::make_unique<evolution::evolution<
			algorithm_t_path, population::population<representations::path>, loggers::basic_logger>>(
			alg, logger, num_gen, pop_size, &points, &gen);
	}
	else
	{
		std::cout << "Unknown crossover algorithm " << cross_alg << std::endl;
		return 1;
	}
	evolution_ptr->run();

	return 0;
}
