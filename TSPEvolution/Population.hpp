#pragma once
#include "Map.hpp"
#include <utility>
#include <vector>
#include "Utility.hpp"
#include "Representations.hpp"
#include <algorithm>
/**
 * \brief Namespace containing classes related to population
 */
namespace population
{
	

	/**
	 * \brief Class encapsulating population logic
	 *		Keeps individuals sorted by fitness.
	 * \tparam TRepresentation Representation used
	 */
	template <typename TRepresentation>
	class population
	{
		std::vector<TRepresentation> individuals_;
	public:
		/**
		 * \brief Constructor
		 * \param individuals Individuals in population 
		 */
		population(std::vector<TRepresentation> individuals): individuals_(std::move(individuals))
		{
			std::sort(individuals_.begin(), individuals_.end(),
			          [](const TRepresentation& individual1, const TRepresentation& individual2)
			          {
				          return individual1.get_fitness() < individual2.get_fitness();
			          });
		}

		using representation = TRepresentation;

		/**
		 * \brief Method to access individual at specified index
		 * \param index Index
		 * \return Const reference to individual
		 */
		const TRepresentation& operator[](std::size_t index) const
		{
			return individuals_[index];
		}

		/**
		 * \brief Method to get population size
		 * \return Number of individual
		 */
		std::size_t size() const /*override*/
		{
			return individuals_.size();
		}

		/**
		 * \brief Method to get begin iterator
		 * \return Iterator
		 */
		auto begin() const
		{
			return individuals_.cbegin();
		}

		/**
		 * \brief Method to get end iterator
		 * \return Iterator
		 */
		auto end() const
		{
			return individuals_.cend();
		}

		/**
		 * \brief Static function to create random generation of given size
		 * \tparam TGenerator Type of generator used
		 * \param count Number of individuals
		 * \param map Pointer to map used
		 * \param generator Pointer to generator used
		 * \return New population
		 */
		template <typename TGenerator>
		static population make_random(std::size_t count, const enviroment::point_set* map, TGenerator* generator)
		{
			std::vector<TRepresentation> individuals;
			for (size_t i = 0; i < count; i++)
			{
				individuals.push_back(TRepresentation::make_random(map->size(), map, *generator));
			}

			return population(individuals);
		}
	};
}
