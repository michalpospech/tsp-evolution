#include "Representations.hpp"
#include <numeric>
#include <algorithm>
#include <list>
#include <utility>
using namespace representations;
using namespace std;


ordinal::ordinal(vector<std::size_t> vertices, const enviroment::point_set* map) : chromosome_(std::move(vertices)),
                                                                                   map_(map),
                                                                                   fitness_(0),
                                                                                   has_fitness_(false)
{
}


double ordinal::get_fitness() const
{
	if (!has_fitness_)
	{
		fitness_ = map_->get_path_length(decode(chromosome_));
	}
	return fitness_;
}

std::vector<std::size_t> ordinal::get_path() const
{
	return ordinal::decode(chromosome_);
}

const std::size_t& ordinal::operator[](std::size_t index) const
{
	return chromosome_[index];
}

std::size_t& ordinal::operator[](std::size_t index)
{
	return chromosome_[index];
}

std::size_t ordinal::size() const
{
	return chromosome_.size();
}

const enviroment::point_set* ordinal::get_map() const
{
	return map_;
}


std::vector<std::size_t> ordinal::decode(const vector<std::size_t>& chromosome)
{
	vector<std::size_t> decoded_vertices;
	list<std::size_t> numbers(chromosome.size());
	iota(numbers.begin(), numbers.end(), 0);
	for (auto&& vertex_order : chromosome)
	{
		auto iterator = numbers.begin();
		for (std::size_t i = 0; i < vertex_order; ++i)
		{
			++iterator;
		}
		decoded_vertices.push_back(*iterator);
		numbers.erase(iterator);
	}
	return decoded_vertices;
}

std::vector<std::size_t> ordinal::encode(const vector<std::size_t>& vertices)
{
	vector<std::size_t> encoded_vertices;
	list<std::size_t> numbers(vertices.size());
	iota(numbers.begin(), numbers.end(), 0);
	for (auto&& vertex_num : vertices)
	{
		auto iterator = numbers.begin();
		uint32_t vertex_order = 0;
		while (*iterator != vertex_num)
		{
			++vertex_order;
			++iterator;
		}
		encoded_vertices.push_back(vertex_order);
		numbers.erase(iterator);
	}
	return encoded_vertices;
}

representations::path::path(std::vector<std::size_t> vertices, const enviroment::point_set* map): chromosome_(
	                                                                                                  std::move(vertices)),
                                                                                                  map_(map),
                                                                                                  fitness_(0),
                                                                                                  has_fitness_(false)
{
}

std::vector<std::size_t> path::get_path() const
{
	return chromosome_;
}

double representations::path::get_fitness() const
{
	if (!has_fitness_)
	{
		fitness_ = map_->get_path_length(chromosome_);
	}
	return fitness_;
}

const std::size_t& representations::path::operator[](std::size_t index) const
{
	return chromosome_[index];
}

std::size_t& representations::path::operator[](std::size_t index)
{
	return chromosome_[index];
}

std::size_t representations::path::size() const
{
	return chromosome_.size();
}

const enviroment::point_set* path::get_map() const
{
	return map_;
}

std::vector<std::size_t> representations::path::decode(const std::vector<std::size_t>& chromosome)
{
	return chromosome;
}

std::vector<std::size_t> representations::path::encode(const std::vector<std::size_t>& vertices)
{
	return vertices;
}
