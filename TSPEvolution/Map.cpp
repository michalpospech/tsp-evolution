﻿#include "Map.hpp"
#include <string>
#include <sstream>
#include <cmath>
#include <utility>
#include <iostream>
#include <exception>
using namespace enviroment;

point::point(double x, double y) : x_(x), y_(y)
{
}

double point::get_x() const
{
	return x_;
}

double point::get_y() const
{
	return y_;
}

point_set::point_set() = default;


std::size_t point_set::size() const
{
	return points_.size();
}

point_set::point_set(std::vector<point> points) : points_(std::move(points))
{
}


double point_set::get_distance(std::size_t index1, std::size_t index2) const
{
	if (index1 >= points_.size() || index2 >= points_.size())
	{
		throw std::runtime_error("Index out of range");
	}
	auto point1 = points_[index1];
	auto point2 = points_[index2];
	auto x_dist = point1.get_x() - point2.get_x();
	auto y_dist = point1.get_y()- point2.get_y();
	return std::sqrt(std::pow(x_dist, 2) + pow(y_dist, 2));
}

double point_set::get_path_length(const std::vector<std::size_t>& path) const
{
	double length = 0;
	for (std::size_t i = 0; i < path.size(); i++)
	{
		length += get_distance(path[i], path[(i + 1) % path.size()]);
	}
	return length;

}

point_set point_set::parse(std::istream& input)
{
	std::vector<point> points;
	std::size_t line_number = 0;
	while (input.good() && !input.eof())
	{
		std::string line;
		++line_number;
		getline(input, line);
		if (!line.empty())
		{
			std::stringstream line_stream(line);
			double x;
			double y;
			try
			{
				std::string value;
				getline(line_stream, value, ' ');
				x = stod(value);
				getline(line_stream, value, ' ');
				y = stod(value);
				if (!line_stream.eof())
				{
					throw std::logic_error("Unexpected characters in the end of line");
				}
			}

			catch (...)
			{
				throw std::runtime_error(std::string("Invalid format at line number ") + std::to_string(line_number));
			}
			points.emplace_back(x, y);
		}
	}
	return point_set(points);
}


