﻿#pragma once
#include <ostream>
#include <numeric>

/**
 * \brief Namespace containing loggers, functors used to print information about evolution
 */
namespace loggers
{
	/**
	 * \brief Class encapsulating printing of information to file/stdout
	 */
	class basic_logger
	{
	private:
		std::ostream& output_;
	public:
		/**
		 * \brief Constructor
		 * \param stream Used stream
		 */
		basic_logger(std::ostream& stream) : output_(stream)
		{
		}

		/**
		 * \brief Method to print information about current generation. Namely average fitness and best (lowest) fitness.
		 * \tparam TPopulation Type of population
		 * \param population Population
		 */
		template <typename TPopulation>
		void operator()(TPopulation&& population)
		{
			double sum = std::accumulate(population.begin(), population.end(), 0.0, [](double left, auto&& right)
			{
				return left + right.get_fitness();
			});
			double avg = sum / population.size();
			output_ << avg << "," << population[0].get_fitness() << std::endl;
		}
		void print_header()
		{
			output_ << "average,best" << std::endl;
		}
	};
}
