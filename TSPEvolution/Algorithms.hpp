#pragma once
#include "Representations.hpp"

#include <mutex>
#include <memory>
#include <type_traits>
#include <algorithm>
#include <utility>
#include <vector>
#include <algorithm>
#include <numeric>
#include <set>

/**
 * \brief Namespace containing functor classes for genetic algorithm
 */
namespace algorithms
{
	/**
	 * \brief Functor class representing one point crossover.
	 *
	 * Chooses random index and performs simple crossover at that index.
	 * Requires \ref representations::ordinal "ordinal" representation of path.
	 * \tparam TGenerator Type of generator used
	 */
	template <typename TGenerator>
	class one_point_crossover
	{
	public:
		/**
		 * \brief Constructor
		 * \param generator Shared pointer to thread safe generator of random numbers
		 * \param len Length of paths to combine
		 */
		one_point_crossover(TGenerator* generator, std::size_t len): generator_(generator),
		                                                             range_(0, len - 1)
		{
		}

		/**
		 * \brief Overloaded () operator.
		 *
		 * Chooses random index and performs simple crossover at that index.
		 * \param parent1 Parent
		 * \param parent2 Parent
		 * \return Offspring
		 */
		representations::ordinal operator()(const representations::ordinal& parent1, const representations::ordinal& parent2)
		{
			std::size_t crossover_index = range_(*generator_);
			std::vector<std::size_t> new_chromosome(parent1.size());
			for (std::size_t i = 0; i < crossover_index; i++)
			{
				new_chromosome[i] = parent1[i];
			}
			for (std::size_t i = crossover_index; i < parent2.size(); i++)
			{
				new_chromosome[i] = parent2[i];
			}
			return representations::ordinal(new_chromosome, parent1.get_map());
		}

	private:
		TGenerator* generator_;
		 std::uniform_int_distribution<std::size_t> range_;
	};

	/**
	 * \brief Functor class representing PMX algorithm.
	 *
	 * Chooses 2 random indices a performs crossover there
	 * Requires \ref representations::path "path" representation of path.
	 * \tparam TGenerator Type of generator used
	 */
	template <typename TGenerator>
	class pmx
	{
	public:
		/**
		* \brief Constructor
		* \param generator Pointer to generator of random numbers
		* \param len Length of paths to combine
		*/
		pmx(TGenerator* generator, std::size_t len): generator_(generator), range_(0, len - 1)
		{
		}

		/**
		* \brief Overloaded () operator.
		*
		* Chooses random index and performs PMX at that index.
		* \param parent1 Parent
		* \param parent2 Parent
		* \return Offspring
		*/
		representations::path operator()(const representations::path& parent1, const representations::path& parent2)
		{
			std::size_t index1 = range_(*generator_);
			std::size_t index2 = range_(*generator_);
			while (index1 == index2)
			{
				index2 = range_(*generator_);
			}
			if (index1 > index2)
			{
				std::swap(index1, index2);
			}
			std::vector<std::size_t> result(parent1.size());
			std::set<std::size_t> crossed_values;
			std::vector<std::size_t> original_values;
			for (size_t i = 0; i < index1; ++i)
			{
				result[i] = parent1[i];
			}
			for (size_t i = index1; i < index2; ++i)
			{
				result[i] = parent2[i];
				crossed_values.insert(parent2[i]);
				original_values.push_back(parent1[i]);
			}
			for (size_t i = index2; i < result.size(); ++i)
			{
				result[i] = parent1[i];
			}
			std::reverse(original_values.begin(), original_values.end());
			size_t index = 0;
			for (size_t i = 0; i < index1; ++i)
			{
				if (crossed_values.count(result[i]) > 0) //is duplicate of crossed value
				{
					while (crossed_values.count(original_values[index]) > 0)
					{
						++index;
					}
					result[i] = original_values[index];
					++index;
				}
			}
			for (size_t i = index2; i < result.size(); ++i)
			{
				if (crossed_values.count(result[i]) > 0)
				{
					while (crossed_values.count(original_values[index]) > 0)
					{
						++index;
					}
					result[i] = original_values[index];
					++index;
				}
			}
			return representations::path(result, parent1.get_map());
		}

	private:
		TGenerator* generator_;
		 std::uniform_int_distribution<std::size_t> range_;
	};


	/**
	 * \brief Functor class representing switch mutation operator.
	 *
	 * Switches vertices at two randomly generated indices.
	 * \tparam TGenerator Type of generator used
	 */
	template <typename TGenerator>
	class vertex_switch
	{
		TGenerator* generator_;
		 std::uniform_int_distribution<std::size_t> range_;
	public:
		/**
		 * \brief Constructor
		 * \param size Size of path to modify
		 * \param generator Shared pointer to thread safe generator of random numbers
		 */
		vertex_switch(std::size_t size, TGenerator* generator) : generator_(generator), range_(0, size - 1)
		{
		}

		/**
		 * \brief Overloaded () operator.
		 *
		 * Switches 2 random vertices on provided individual.
		 * \param individual Individual
		 * \tparam TRepresentation Representation of path used
		 * \return Modified individual
		 * 
		 */
		template <typename TRepresentation>
		TRepresentation operator()(const TRepresentation& individual)
		{
			auto path = individual.TRepresentation::get_path();
			std::size_t index1 = range_(*generator_);
			std::size_t index2 = range_(*generator_);
			std::swap(path[index1], path[index2]);
			return TRepresentation(TRepresentation::encode(path), individual.get_map());
		}
	};

	/**
	 * \brief Functor class representing tournament selector.
	 * Selects n random individuals from population, orders them by fitness and chooses one element with geometric probability
	 * \tparam TGenerator Used generator type
	 */
	template <typename TGenerator>
	class tournament_selector
	{
		TGenerator* bit_generator_;
		std::geometric_distribution<std::size_t> winner_generator_;

		std::size_t tournament_size_;
	public:
		tournament_selector(std::size_t size, double param, TGenerator* bit_generator) :
			bit_generator_(bit_generator),
			winner_generator_(param),
			tournament_size_(size)
		{
		}

		/**
		 * \brief Chooses one random individual
		 * \tparam TPopulation Type of population
		 * \param population Population to select from
		 * \return Chosen individual
		 */
		template <typename TPopulation>
		const typename TPopulation::representation& operator()(const TPopulation& population)
		{
			std::vector<std::size_t> nums(population.size());
			std::iota(nums.begin(), nums.end(), 0);
			std::vector<std::size_t> tournament;
			std::sample(nums.begin(), nums.end(), std::back_inserter(tournament), tournament_size_, *bit_generator_);
			//sorting selected individuals
			std::sort(tournament.begin(), tournament.end(), [&population](auto&& left, auto&& right)
			{
				return population[left].get_fitness() < population[right].get_fitness();
			});
			//choosing winner
			std::size_t winner_index = winner_generator_(*bit_generator_);
			winner_index = winner_index < tournament.size() ? winner_index : tournament.size() - 1;
			return population[tournament[winner_index]];
		}
	};

	/**
	 * \brief Functor class wrapping logic of generation of new generation
	 * \tparam TExecution Type of executor
	 * \tparam TSelection Type of selector
	 * \tparam TCrossover Crossover algorithm used
	 * \tparam TMutation Mutator used
	 * \tparam TGenerator Type of generator used
	 */
	template <typename TExecution, typename TSelection, typename TCrossover, typename TMutation, typename TGenerator>
	class renew_all_algorithm
	{
	public:
		/**
		 * \brief Constuctor 
		 * \param bit_generator Pointer to used generator 
		 * \param crossover_chance Chance of crossover
		 * \param mutation_chance Chance of mutation
		 * \param executor Exectution policy
		 * \param selector Selection policy
		 * \param crossover Crossover algorithm
		 * \param mutator Mutation algorithm
		 */
		renew_all_algorithm(TGenerator* bit_generator, double crossover_chance, double mutation_chance,
		                    const TExecution& executor, const TSelection& selector, const TCrossover& crossover,
		                    const TMutation& mutator) : generator_(0, 1),
		                                         bit_generator_(bit_generator),
		                                         crossover_chance_(crossover_chance),
		                                         mutation_chance_(mutation_chance),
		                                         executor_(executor),
		                                         selector_(selector),
		                                         crossover_(crossover),
		                                         mutator_(mutator)
		{
		};

		/**
		 * \brief Method to create new generation of poupulation
		 * \tparam TPopulation Type of population
		 * \param population Population
		 * \return New population
		 */
		template <typename TPopulation>
		auto operator()(const TPopulation& population)
		{
			auto procedure = [this](
				TPopulation population)
			{
				return this->generate_offspring(population);
			};
			std::vector<decltype(std::declval<TExecution>()(procedure, population))>
				new_individuals_wrappers;
			for (size_t offspring_num = 0; offspring_num < population.size(); offspring_num++)
			{
				new_individuals_wrappers.emplace_back(executor_(procedure, population));
			}
			std::vector<typename TPopulation::representation> new_individuals;
			new_individuals.reserve(new_individuals_wrappers.size());
			for (auto&& individual : new_individuals_wrappers)
			{
				new_individuals.emplace_back(individual.get());
			}
			return TPopulation(new_individuals);
		}

	private:
		/**
		 * \brief Helper method generating one offspring from whole population
		 * \tparam TPopulation Type of population
		 * \param population Population to choose from
		 * \return New individual
		 */
		template <typename TPopulation>
		auto generate_offspring(const TPopulation& population)
		{
			auto&& parent1 = selector_(population);
			auto&& parent2 = selector_(population);
			typename TPopulation::representation offspring((generator_(*bit_generator_) < crossover_chance_)
				                                               ? crossover_(parent1, parent2)
				                                               : parent1);

			if (generator_(*bit_generator_) < mutation_chance_)
			{
				offspring = mutator_(offspring);
			}
			offspring.get_fitness();
			return offspring;
		}

		std::uniform_real_distribution<double> generator_;
		TGenerator* bit_generator_;
		double crossover_chance_;
		double mutation_chance_;
		TExecution executor_;
		TSelection selector_;
		TCrossover crossover_;
		TMutation mutator_;
	};
}
