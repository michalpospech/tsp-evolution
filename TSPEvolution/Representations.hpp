#pragma once
#include "Map.hpp"
#include <vector>
#include <random>
#include "Utility.hpp"
#include <numeric>

/**
 * \brief Namespace containing various representations of path.
 */
namespace representations
{
	/**
	 * \brief Ordinal representation of path.
	 *
	 * Represents path with ordinal representation specified <a href="https://iccl.inf.tu-dresden.de/w/images/b/b7/GA_for_TSP.pdf" > here<a/>.
	 */
	class ordinal
	{
		std::vector<std::size_t> chromosome_;
		const enviroment::point_set* map_;
		double mutable fitness_;
		bool mutable has_fitness_;
	public:
		/**
		 * \brief Constructor
		 * \param vertices Vector of vertex numbers
		 * \param map Pointer to map
		 */
		ordinal(std::vector<std::size_t> vertices, const enviroment::point_set* map);

		/**
		 * \brief Gets represented path as vector of vertex numbers
		 * \return Vector of vertex numbers
		 */
		std::vector<std::size_t> get_path() const;
		/**
		 * \brief Gets fitness (length of path) of the individual
		 *
		 *  If it has not been calculated yet, the fitness gets calculated and saved.
		 * \return Fitness
		 */
		double get_fitness() const;
		/**
		 * \brief Accesses element at specified index
		 * \param index Index
		 * \return Constant reference to element
		 */
		const std::size_t& operator[](std::size_t index) const;
		/**
		* \brief Accesses element at specified index
		* \param index Index
		* \return Reference to element
		*/
		std::size_t& operator[](std::size_t index);
		/**
		 * \brief Get size of the individual (number of edges in path)
		 * \return Size
		 */
		std::size_t size() const;
		/**
		 * \brief Gets pointer to map
		 * \return Pointer to map
		 */
		const enviroment::point_set* get_map() const;

		/**
		 * \brief Creates random individual of specified size.
		 *
		 * The individual gets generated using provided thread safe generator and pointer to map
		 * \param size Size
		 * \param map Pointer to map
		 * \param generator Reference to generator
		 * \tparam TGenerator Type of generator used
		 * \return New random indiviual
		 */
		template <typename TGenerator>
		static ordinal make_random(std::size_t size, const enviroment::point_set* map, TGenerator& generator)
		{
			std::vector<std::size_t> vertices(size);
			std::iota(vertices.begin(), vertices.end(), 0);
			shuffle(vertices.begin(), vertices.end(), generator);
			return ordinal(encode(vertices), map);
		}

		/**
		 * \brief Converts path from ordinal representation to vector of vertex numbers
		 * \param chromosome Ordinal representation
		 * \return Vector of vertex numbers
		 */
		static std::vector<std::size_t> decode(const std::vector<std::size_t>& chromosome);
		/**
		* \brief Converts path from vector of vertex numbers to ordinal representation
		* \param vertices Vector of vertex numbers
		* \return Ordinal representation
		*/
		static std::vector<std::size_t> encode(const std::vector<std::size_t>& vertices);
	};

	/**
	* \brief Path representation of path.
	*
	* Represents path with path representation specified <a href="https://iccl.inf.tu-dresden.de/w/images/b/b7/GA_for_TSP.pdf" > here<a/>.
	*/
	class path
	{
		std::vector<std::size_t> chromosome_;
		const enviroment::point_set* map_;
		double mutable fitness_;
		bool mutable has_fitness_;
	public:
		/**
		* \brief Constructor
		* \param vertices Vector of vertex numbers
		* \param map Pointer to map
		*/
		path(std::vector<std::size_t> vertices, const enviroment::point_set* map);

		/**
		* \brief Gets represented path as vector of vertex numbers
		* \return Vector of vertex numbers
		*/
		std::vector<std::size_t> get_path() const;
		/**
		* \brief Gets fitness (length of path) of the individual
		*
		*  If it has not been calculated yet, the fitness gets calculated and saved.
		* \return Fitness
		*/
		double get_fitness() const;
		/**
		* \brief Accesses element at specified index
		* \param index Index
		* \return Constant reference to element
		*/
		const std::size_t& operator[](std::size_t index) const;
		/**
		* \brief Accesses element at specified index
		* \param index Index
		* \return Reference to element
		*/
		std::size_t& operator[](std::size_t index);
		/**
		* \brief Get size of the individual (number of edges in path)
		* \return Size
		*/
		std::size_t size() const;
		/**
		* \brief Gets pointer to map
		* \return Pointer to map
		*/
		const enviroment::point_set* get_map() const;

		/**
		* \brief Creates random individual of specified size.
		*
		* The individual gets generated using provided thread safe generator and pointer to map
		* \param size Size
		* \param map Pointer to map
		* \param generator Reference to generator
		* \tparam TGenerator Type of generator used
		* \return New random indiviual
		*/
		template <typename TGenerator>
		static path make_random(std::size_t size, const enviroment::point_set* map, TGenerator& generator)
		{
			std::vector<std::size_t> vertices(size);
			std::iota(vertices.begin(), vertices.end(), 0);
			shuffle(vertices.begin(), vertices.end(), generator);
			return path(vertices, map);
		}

		/**
		* \brief Converts path from ordinal representation to vector of vertex numbers
		* \param chromosome Path representation
		* \return Vector of vertex numbers
		*/
		static std::vector<std::size_t> decode(const std::vector<std::size_t>& chromosome);
		/**
		* \brief Converts path from vector of vertex numbers to ordinal representation
		* \param vertices Vector of vertex numbers
		* \return Path representation
		*/
		static std::vector<std::size_t> encode(const std::vector<std::size_t>& vertices);
	};
}
