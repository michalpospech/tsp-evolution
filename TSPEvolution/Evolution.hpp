#pragma once
#include "Representations.hpp"

/**
 * \brief Namespace containing classes encapsulating evolution logic
 */
namespace evolution
{
	/**
	 * \brief Abstract class representing evoltion 
	 */
	class evolution_base
	{
	public:
		/**
		 * \brief Destructor
		 */
		virtual ~evolution_base() = default;
		/**
		 * \brief Method that runs the evolution
		 */
		virtual void run() = 0;
		/**
		 * \brief Method that returns path with best fitness
		 * \return Best path
		 */
		virtual std::vector<std::size_t> get_best() = 0;
	};

	/**
	 * \brief Class encapsulating evolutionary algorithm
	 * \tparam TAlgorithm Type of algorithm used
	 * \tparam TPopulation Type of population
	 * \tparam TLogger Type of logger used
	 */
	template <typename TAlgorithm, typename TPopulation, typename TLogger>
	class evolution : public evolution_base

	{
		TAlgorithm algorithm_;
		TPopulation population_;
		TLogger logger_;
		std::size_t num_generations_;
		std::size_t pop_size_;
	public:
		/**
		 * \brief Constructor
		 * \tparam TGenerator Type of used generator
		 * \param algorithm Algorithm
		 * \param logger Logger
		 * \param num_generations Number of generations 
		 * \param pop_size Population size
		 * \param map Pointer to map
		 * \param generator Pointer to generator
		 */
		template <typename TGenerator>
		evolution(const TAlgorithm& algorithm, const TLogger& logger, std::size_t num_generations,
		          std::size_t pop_size, const enviroment::point_set* map, TGenerator* generator)
			: algorithm_(algorithm), population_(TPopulation::make_random(pop_size, map, generator)
			  ),
			  logger_(logger),
			  num_generations_(num_generations),
			  pop_size_(pop_size)
		{
		}


		/**
		 * \copydoc 
		 */
		virtual void run() override
		{
			logger_.print_header();
			logger_(population_);
			for (size_t gen = 0; gen < num_generations_; ++gen)
			{
				population_ = algorithm_(population_);
				logger_(population_);
			}
		}

		/**
		 *\copydoc 
		 */
		std::vector<std::size_t> get_best() override
		{
			return population_[0].get_path();
		};
	};
}
