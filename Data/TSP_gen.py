import random
import numpy as np
import sys


num = int(sys.argv[1])
file_name = sys.argv[2]

values = (np.random.rand(num, 2) - 0.5) * 100
out_file = open(file_name, mode='w+')
for value in values:
    out_file.write(f"{value[0]} {value[1]}\n")
out_file.close()
